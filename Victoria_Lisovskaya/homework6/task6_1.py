a = "4561261212345467"


# упрощенный алгоритм "Луна"
def validate(number: str):
    s = 0
    revert = number[::-1]
    for i in range(0, len(revert)):
        if i % 2 == 0:
            s += int(revert[i])
        else:
            if int(revert[i]) * 2 > 9:
                # находим сумму цифр в числе
                first_number = (int(revert[i]) * 2) % 10
                second_number = (int(revert[i]) * 2) // 10
                s += first_number + second_number
            else:
                s += int(revert[i]) * 2
    return s % 10 == 0


print(validate(a))
