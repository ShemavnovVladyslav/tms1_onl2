numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
L = [x * -1 if x <= 0 else x for x in numbers]
print(L)

sentence = 'the quick brown fox jumps over the lazy dog'
a = sentence.split()
for i in a:
    if i == 'the':
        continue
    print(f'Длина слова: {i}: {len(i)}')
