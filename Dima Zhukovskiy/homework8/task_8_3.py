def decorator(f):

    def wrapper(a):
        lst = a.split()
        c = []
        d = []
        for i in lst:
            if i == '+' or i == '*' or i == '-' or i == '/' or i == '**':
                d.append(i)
            elif i != '+' and i != '*' and i != '-' and i != '/' and i != '**':
                c.append(i)
        a = c + d
        s = []
        for x in a:
            if x == '+':
                r = int(s[1]) + int(s[0])
                s = [r] + s[2:]
            elif x == '-':
                r = int(s[1]) - int(s[0])
                s = [r] + s[2:]
            elif x == '*':
                r = int(s[1]) * int(s[0])
                s = [r] + s[2:]
            elif x == '/':
                r = int(s[1]) // int(s[0])
                s = [r] + s[2:]
            else:
                s = [x] + s
        f(print(s[0]))

    return wrapper


@decorator
def cal(ex):
    return ex


expr = input('Введите выражение: ')

cal(expr)
