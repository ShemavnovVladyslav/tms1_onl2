def words(word: str):
    # строка с первой буквой слова
    res = word[0]
    # счетчик повторений букв в слове
    count = 0
    for i in range(len(word) - 1):
        # если первая буква равна с последующей
        if word[i] == word[i + 1]:
            # добавляем в счетчик +1
            count += 1
        else:
            # если счетчик больше нуля
            # записываем данные счетчика в строку
            # и обнуляем
            if count > 0:
                # добавляю к счетчику + 1
                # чтобы учесть первое появляение буквы
                res += str(count + 1)
                count = 0
            # если первая буква неравна с последующей
            # записываем следующую букву
            res += word[i + 1]
    print(res)


words("aaabbvb")
words("cccbbca")
words("cccbba")
words("abeehhhhhccced")
words("aaabbceedd")
words("abcde")
words("aaabbdefffff")
