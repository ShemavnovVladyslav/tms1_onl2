from pages.main_page import MainPage
from pages.login_page import LoginPage
from pages.cart_page import CartPage
from locators.links_of_pages import LinksForPages
from locators.user_data import UserData


def test_check_empty_cart(browser):
    cart_page = CartPage(browser)
    main_page = MainPage(browser)
    main_page.open_page(url=LinksForPages.MAINPAGELINK)
    main_page.open_cart_page()
    cart_page.check_empty_cart_on_cart_page()


def test_check_login_page(browser):
    login_page = LoginPage(browser)
    main_page = MainPage(browser)
    main_page.open_page(url=LinksForPages.MAINPAGELINK)
    main_page.open_login_page()
    login_page.check_current_page(url=LinksForPages.LOGINPAGELINK)


def test_success_delete_product_from_cart(browser):
    cart_page = CartPage(browser)
    main_page = MainPage(browser)
    main_page.open_page(url=LinksForPages.MAINPAGELINK)
    main_page.add_things_in_the_cart()
    main_page.go_to_checkout()
    cart_page.delete_product_from_cart()
    cart_page.check_empty_cart_on_cart_page()


def test_success_search_product(browser):
    main_page = MainPage(browser)
    main_page.open_page(url=LinksForPages.MAINPAGELINK)
    main_page.search_product(product_name="Blouse")


def test_error_after_fail_login(browser):
    login_page = LoginPage(browser)
    login_page.open_page(url=LinksForPages.LOGINPAGELINK)
    login_page.sign_in(email=UserData.FAIL_EMAIL, password=UserData.PASSWORD)
    login_page.fail_login()
