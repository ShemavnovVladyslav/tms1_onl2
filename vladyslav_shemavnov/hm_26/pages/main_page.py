from pages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from locators.main_page_locators import MainPageLocators
import time


class MainPage(BasePage):

    def check_empty_cart_from_main_page(self):
        assert self.easy_find_element(
            MainPageLocators.EMPTY_CART_MAIN_PAGE
        ).text == '(empty)'

    def add_things_in_the_cart(self):
        dress = self.easy_find_element(
            MainPageLocators.DRESS_LOCATOR
        )
        dress.click()

        add_button = self.easy_find_element(
            MainPageLocators.ADD_TO_CART_BUTTON
        )
        add_button.click()

    def go_to_checkout(self):
        checkout_button = self.easy_find_element(
            MainPageLocators.CHECKOUT_BUTTON
        )
        checkout_button.click()

    def close_window(self):
        close_button = self.easy_find_element(
            MainPageLocators.CLOSE_POPUP_BUTTON
        )
        close_button.click()

    def check_things_in_cart(self):
        time.sleep(2)
        cart = self.easy_find_element(MainPageLocators.CART)
        hov = ActionChains(self.browser).move_to_element(cart)
        hov.perform()

        assert self.easy_find_element(
            MainPageLocators.THINGS_LIST_IN_CART
        ).is_displayed()

    def open_cart_page(self):
        cart = self.easy_find_element(MainPageLocators.CART)
        cart.click()

    def open_login_page(self):
        sign_button = self.easy_find_element(
            MainPageLocators.sing_button_locator
        )
        sign_button.click()

    def search_product(self, product_name):
        search_field = self.easy_find_element(
            MainPageLocators.SEARCH_FIELD
        )
        search_field.send_keys(product_name)
        search_button = self.easy_find_element(
            MainPageLocators.SEARCH_BUTTON
        )
        search_button.click()
        xpath = f"//div[@id='center_column']" \
                f"//ul" \
                f"//li" \
                f"//a[@class='product-name' and contains(., '{product_name}')]"
        assert self.easy_find_element((By.XPATH, xpath)).is_displayed()
