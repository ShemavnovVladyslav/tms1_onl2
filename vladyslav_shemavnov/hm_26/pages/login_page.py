from pages.base_page import BasePage
from locators.login_page_locators import LoginPageLocators


class LoginPage(BasePage):
    def sign_in(self, email, password):
        email_field = self.easy_find_element(LoginPageLocators.EMAIL_LOCATOR)
        email_field.send_keys(email)
        password_field = self.easy_find_element(LoginPageLocators
                                                .PASSWORD_LOCATOR)
        password_field.send_keys(password)
        sing_in_button = self.easy_find_element(LoginPageLocators
                                                .SIGN_IN_BUTTON)
        sing_in_button.click()

    def fail_login(self):
        assert self.easy_find_element(LoginPageLocators.FAIL_LOGIN_LOCATOR)
