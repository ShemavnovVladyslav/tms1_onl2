from pages.base_page import BasePage
from locators.cart_page_locators import CartPageLocators


class CartPage(BasePage):
    def check_empty_cart_on_cart_page(self):
        assert self.easy_find_element(CartPageLocators.EMPTY_CART_LOCATOR)

    def delete_product_from_cart(self):
        delete_button = self.easy_find_element(CartPageLocators.
                                               DELETE_PRODUCT_LOCATOR)
        delete_button.click()
