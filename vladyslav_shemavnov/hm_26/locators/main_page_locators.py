from selenium.webdriver.common.by import By


class MainPageLocators:
    sing_button_locator = (By.CLASS_NAME, "login")
    DRESS_LOCATOR = (By.XPATH, "//div[@id='center_column']//ul//li[5]")
    EMPTY_CART_MAIN_PAGE = (By.XPATH, "//div[@class='shopping_cart']"
                                      "/a/span[@class='ajax_cart_no_product']")
    ADD_TO_CART_BUTTON = (By.XPATH, "//p[@id='add_to_cart']//button")
    CHECKOUT_BUTTON = (By.XPATH, "//span[contains(., 'Proceed to checkout')]")
    CLOSE_POPUP_BUTTON = (By.CLASS_NAME, "cross")
    CART = (By.XPATH, "//div[@class='shopping_cart']//a")
    THINGS_LIST_IN_CART = (By.XPATH, "//div[@class='shopping_cart']"
                                     "//div[@class='cart_block_list']")
    SEARCH_FIELD = (By.ID, "search_query_top")
    SEARCH_BUTTON = (By.NAME, "submit_search")
