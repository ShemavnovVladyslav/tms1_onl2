from selenium.webdriver.common.by import By


class LoginPageLocators:
    EMAIL_LOCATOR = (By.ID, "email")
    PASSWORD_LOCATOR = (By.ID, "passwd")
    SIGN_IN_BUTTON = (By.ID, "SubmitLogin")
    fail_login_xpath = "//div[@class='alert alert-danger']" \
                       "//li[contains(., 'Authentication failed.')]"
    FAIL_LOGIN_LOCATOR = (By.XPATH, fail_login_xpath)
