from selenium.webdriver.common.by import By


class CartPageLocators:
    EMPTY_CART_LOCATOR = (By.XPATH,
                          "//p[contains(., "
                          "'Your shopping cart is empty.')]")
    DELETE_PRODUCT_LOCATOR = (By.CLASS_NAME, "cart_quantity_delete")
