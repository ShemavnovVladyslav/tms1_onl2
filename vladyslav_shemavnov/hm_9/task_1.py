class Book:
    def __init__(self, name, author,
                 pages, isbn, flag):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.flag = flag
        self.reserved = True

    def check_book_status(self):
        """ Метод проверки статуса книги """
        if self.reserved:
            print("Книга сейчас доступна")
            return True
        else:
            print("К сожалению книга уже занята")
            return False


class Person:
    def __init__(self, name):
        self.name = name

    def get_book(self, book):
        """ Метод для взятия книги в библиотеке """
        if book.check_book_status():
            print(f"Поздравляем {self.name} "
                  f"вы взяли книгу {book.name}")
            book.reserved = False

    def reserve_book(self, book):
        """ Метод для брони книги """
        if book.check_book_status():
            print(f"Поздравляем {self.name} "
                  f"вы забронировали книгу {book.name}")
            book.reserved = False

    def return_book(self, book):
        """ Метод возврата книги """
        book.reserved = True
        print(f"Спасибо {self.name} "
              f"что вернули книгу {book.name}")


potter = Book(
    name="Harry Potter",
    author="J. K. Rowling",
    pages=332,
    isbn="0-7475-3269-9",
    flag="UK")


hobbit = Book(
    name="Hobbit",
    author="J. R. R. Tolkien",
    pages=310,
    isbn="0044403372",
    flag="UK")


witcher = Book(
    name="The Last Wish",
    author="Andrzej Sapkowski",
    pages=288,
    isbn="978-0-575-08244-1",
    flag="PL"
)


person_1 = Person(name="John")
person_2 = Person(name="Clark")


person_1.get_book(book=witcher)
person_2.get_book(book=witcher)


person_2.reserve_book(book=potter)
person_1.reserve_book(book=potter)


person_2.get_book(book=hobbit)
person_2.return_book(book=hobbit)
person_1.reserve_book(book=hobbit)
