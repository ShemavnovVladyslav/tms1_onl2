"""Объединить словари, значения поместить в список, если нет ключа -> None."""

a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
c = sorted(set(a).union(b))
result = {i: [a.get(i, None), b.get(i, None)] for i in c}
print(result)
