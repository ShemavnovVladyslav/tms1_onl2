"""Напечатайте: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”."""

lst, string1, string2 = ["Ivan", "Ivanou"], "Minsk", "Belarus"
print(f"Привет, {lst[0]} {lst[1]}! Добро пожаловать в {string1} {string2}")
