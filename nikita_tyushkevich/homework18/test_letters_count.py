from unittest import TestCase
from unittest import expectedFailure
from main_letters_num import letters_count


class TestCases(TestCase):

    def test_same_letter(self):
        param = letters_count('aaaa')
        ret_value = 'a4'
        self.assertEqual(param, ret_value)

    def test_simple_count(self):
        param = letters_count('cccbba')
        ret_value = 'c3b2a'
        self.assertEqual(param, ret_value)

    def test_one_letter_multiple_times(self):
        param = letters_count('abeehhhhhccced')
        ret_value = 'abe2h5c3ed'
        self.assertEqual(param, ret_value)

    def test_two_letters_multiple_times(self):
        param = letters_count('abeehhhhhccceda')
        ret_value = 'abe2h5c3eda'
        self.assertEqual(param, ret_value)

    def test_special_chars(self):
        param = letters_count('!!!$#@%%%%')
        ret_value = '!3$#@%4'
        self.assertEqual(param, ret_value)

    def test_numbers(self):
        param = letters_count('55517799002')
        ret_value = '5317292022'
        self.assertEqual(param, ret_value)

    def test_hieroglyph(self):
        param = letters_count('拉进群')
        ret_value = '拉进群'
        self.assertEqual(param, ret_value)

    @expectedFailure
    def test_int(self):
        param = letters_count(55517)
        ret_value = '5317'
        self.assertEqual(param, ret_value)

    @expectedFailure
    def test_float(self):
        param = letters_count(555.17)
        ret_value = '53.17'
        self.assertEqual(param, ret_value)

    @expectedFailure
    def test_arithmetic(self):
        param = letters_count(555 + 18)
        ret_value = '53 + 18'
        self.assertEqual(param, ret_value)

    @expectedFailure
    def test_bool(self):
        param = letters_count(True)
        ret_value = 'True'
        self.assertTrue(param, ret_value)

    @expectedFailure
    def test_list(self):
        param = letters_count([2, "num"])
        ret_value = '[2, "num"]'
        self.assertTrue(param, ret_value)
