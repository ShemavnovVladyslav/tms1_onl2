from collections import Counter
# 1
text1 = Counter("a-z")
most1 = text1.most_common(1)
print(most1)
# 2
text2 = Counter("Hello World!")
most2 = text2.most_common(1)
print(most2)
# 3
text3 = Counter("How do you do?")
most3 = text3.most_common(1)
print(most3)
# 4
text5 = Counter("Oops!")
most5 = text5.most_common(1)
print(most5)
# 5
a = "AAaooo!!!!"
b = Counter(a).most_common()

for k, v in b:
    if v == 1:
        print(k)
# 6
text7 = Counter("a" * 9000 + "b" * 1000)
most7 = text7.most_common(1)
print(most7)
